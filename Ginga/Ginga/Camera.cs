﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

class Camera
{
    public float scale;
    public Matrix transform;
    public Vector2 position;
    public float angle;
 
    public Camera(Vector2 loc)
    {
        scale = 1.0f;
        angle = 0.0f;
        position = loc;
    }

    public Matrix getTransformation()
    {
        Matrix shift = Matrix.CreateTranslation(new Vector3(-position.X, -position.Y, 0));
        Matrix times = Matrix.CreateScale(scale);
        Matrix rotate = Matrix.CreateRotationZ(angle);
        //Matrix view = Matrix.CreateTranslation(new Vector3(graphicsDevice.Viewport.Width * 0.5f, graphicsDevice.Viewport.Height * 0.5f, 0));
        transform = shift * times * rotate; //* view;
        return transform;
    }
}
