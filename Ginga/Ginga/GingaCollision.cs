﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ginga
{
    class GingaCollision : Collision
    {
        public GingaCollision(Rectangle world, Rectangle screen, Rectangle camera)
            :base(world, screen, camera)
        {
        }

        public Entity collideClosest(Circle circle, bool enemy)
        {
            Entity closest = null;
            List<Entity> selected = collide(circle);
            selected.RemoveAll(
                    delegate(Entity ent)
                    {
                        return ((Unit)ent).enemy == enemy;
                    });
            if (selected.Count == 0)
                return closest;
            else if (selected.Count == 1)
                return selected[0];
            else
            {
                float closestDistance = float.MaxValue;
                foreach (Entity ent in selected)
                {
                    float distance = Vector2.Distance(ent.position, circle.Center);
                    if (closest == null || distance < closestDistance)
                    {
                        closest = ent;
                        closestDistance = distance;
                    }

                }
            }
            return closest;
        }
    }
}
