﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

class Button
{
    Texture2D spriteSheet;
    Vector2 position;
    //Rectangle srcRect;
    Input mouse;
    public Button(Texture2D texture, Vector2 loc, Input mos)
    {
        spriteSheet = texture;
        position = loc;
        mouse = mos;
    }

    public bool clicked()
    {
        return mouse.lMouseButtonClicked() && new Rectangle((int)position.X, (int)position.Y, spriteSheet.Width, spriteSheet.Height).Contains((int)mouse.mousePos().X, (int)mouse.mousePos().Y);
    }

    public void Draw(SpriteBatch spriteBatch)
    {
        spriteBatch.Draw(spriteSheet, position, Color.White);
    }

}
