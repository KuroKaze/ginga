﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ginga
{
    class Unit : Entity
    {
        public Vector2 heading, facing, enemyFacing;
        public float speed = 2.0f, rotateSpeed = 1.0f, turning;
        public bool enemy, withinRange, alive;
        public Circle range;
        public int hp;


        public Unit(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, bool eny)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc)
        {
            heading = position;
            facing = position;
            enemyFacing = position;
            enemy = eny;
            range = new Circle(position, 125.0f);
            withinRange = false;
            hp = 5000;
            alive = true;
        }

        public void moveTo(Vector2 point)
        {
            heading = point;
            turnTo(point);
        }

        public void turnTo(Vector2 point)
        {
            facing = point;
            Vector2 direction = new Vector2(point.X - position.X, point.Y - position.Y);
            turning = (float)Math.Atan2((double)direction.Y, (double)direction.X) + MathHelper.ToRadians(90);
            if (0.0f > turning)
                turning += MathHelper.ToRadians(360);
        }
        public void turnToEnemy(Vector2 point)
        {
            enemyFacing = point;
            Vector2 direction = new Vector2(point.X - position.X, point.Y - position.Y);
            turning = (float)Math.Atan2((double)direction.Y, (double)direction.X) + MathHelper.ToRadians(90);
            if (0.0f > turning)
                turning += MathHelper.ToRadians(360);
        }

        public void attack(Entity attacked)
        {
            Vector2 vect = position - attacked.position;
            float angle = (float)Math.Atan2((double)vect.Y, (double)vect.X);
            vect = new Vector2((float)Math.Cos((double)angle) * range.Radius, (float)Math.Sin((double)angle) * range.Radius);
            moveTo(vect + attacked.position);
        }

        public override void Update(GameTime gameTime)
        {
            if (hp <= 0)
                alive = false;
            if (alive)
            {
                if (enemy)
                    compUpdate();
                else
                    playerUpdate();
            }                
        }

        protected void playerUpdate()
        {
            Vector2 direction = new Vector2(heading.X - position.X, heading.Y - position.Y);
            if ((int)direction.X != 0 || (int)direction.Y != 0)
                move();
            else if (withinRange)
            {
                facing = enemyFacing;
                turn();
            }
            else
            {
                newAnimation(0, 1);
                speed = 2.0f;
            }
        }

        protected void compUpdate()
        {
            Vector2 direction = new Vector2(heading.X - position.X, heading.Y - position.Y);
            if (withinRange)
            {
                facing = enemyFacing;
                turn();
            }
            else if ((int)direction.X != 0 || (int)direction.Y != 0)
                move();
            else
            {
                newAnimation(0, 1);
                speed = 2.0f;
            }
        }


        protected void turn()
        {

            Vector2 direction = new Vector2(facing.X - position.X, facing.Y - position.Y);
            turning = (float)Math.Atan2((double)direction.Y, (double)direction.X) + MathHelper.ToRadians(90);
            if (0.0f > turning)
                turning += MathHelper.ToRadians(360);
            if (Math.Abs(turning - angle) >= MathHelper.ToRadians(rotateSpeed))
            {
                newAnimation(0, 2);
                if (turning < angle)
                    turning += MathHelper.ToRadians(360);
                if ((turning - angle) < MathHelper.ToRadians(180))
                    transform(0, 0, rotateSpeed);
                else
                    transform(0, 0, -rotateSpeed);
            }
            else
            {
                newAnimation(0, 1);
                speed = 2.0f;
            }
        }

        protected void move()
        {
            Vector2 headingDirection = new Vector2(heading.X - position.X, heading.Y - position.Y);
            Vector2 facingDirection = new Vector2(facing.X - position.X, facing.Y - position.Y);
            if ((int)headingDirection.X != 0 || (int)headingDirection.Y != 0)
            {
                float turning = (float)Math.Atan2((double)facingDirection.Y, (double)facingDirection.X) + MathHelper.ToRadians(90);
                if (0.0f > turning)
                    turning += MathHelper.ToRadians(360);
                if (Math.Abs(turning - angle) > MathHelper.ToRadians(rotateSpeed))
                    turn();
                else
                {
                    newAnimation(0, 2);
                    if (headingDirection.X == 1 || headingDirection.Y == 1)
                        speed--;
                    transformForward(speed);
                }
            }
            else
            {
                newAnimation(0, 1);
                speed = 2.0f;
            }
        }

        public override void transformForward(float forward, float times = 0.0f)
        {
            base.transformForward(forward, times);
            range.Center = position;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Draw(spriteSheet, new Vector2(position.X - 25 / 2, position.Y + 25 / 2), new Rectangle((int)Math.Ceiling((5000 - hp) / 200.0), 25, 25, 25), Color.White);
        }
    }
}



public struct Circle
{
    private Vector2 v;
    private Vector2 direction;
    private float distanceSquared;

    public Vector2 Center;
    public float Radius;

    public Circle(Vector2 position, float radius)
    {
        distanceSquared = 0f;
        direction = Vector2.Zero;
        v = Vector2.Zero;
        Center = position;
        Radius = radius;
    }

    public bool Intersects(Rectangle rectangle)
    {
        v = new Vector2(MathHelper.Clamp(Center.X, rectangle.Left, rectangle.Right),
                                MathHelper.Clamp(Center.Y, rectangle.Top, rectangle.Bottom));

        direction = Center - v;
        distanceSquared = direction.LengthSquared();

        return ((distanceSquared > 0) && (distanceSquared < Radius * Radius));
    }
    public bool Intersects(Circle circle)
    {
        float distance = Vector2.Distance(Center, circle.Center);
        return distance > 2 * Radius;
    }
}

