﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

class Interface
{
    protected List<Texture2D> backgrounds;
    protected List<Vector2> positions;
    protected List<SpriteFont> fonts;
    protected List<Entity> entities;
    protected List<String> text;
    protected List<Button> buttons;
    protected Rectangle enviBox;
    protected Input input;
    protected Camera camera;
    protected Collision collision;

    public Interface(Texture2D environment, Rectangle environmentBox, Input inp, Camera cam, Collision colli)
    {
        backgrounds = new List<Texture2D>();
        backgrounds.Add(environment);
        positions = new List<Vector2>();
        positions.Add(Vector2.Zero);
        fonts = new List<SpriteFont>();
        fonts.Add(null);
        entities = new List<Entity>();
        text = new List<String>();
        text.Add(null);
        buttons = new List<Button>();

        enviBox = environmentBox;

        input = inp;
        camera = cam;
        collision = colli;
    }

    public void addComponent(Texture2D texture, Vector2 loc, SpriteFont font)
    {
        backgrounds.Add(texture);
        positions.Add(loc);
        fonts.Add(font);
        text.Add("");
    }

    public void addEntityToComponent(Entity ent)
    {
        entities.Add(ent);
    }

    public void addButton(Texture2D sprite, Vector2 loc)
    {
        buttons.Add(new Button(sprite, loc, input));
    }

    public void removeEnity(Entity ent)
    {
        entities.Remove(ent);
    }

    public void removeEntities()
    {
        entities.Clear();
    }

    public void updateText(int compNum, String str)
    {
        text[compNum] = str;
    }

    public virtual void drawEntities(SpriteBatch spriteBatch)
    {

    }

    public virtual void Update()
    {
        if (input.keyPressed(Keys.Escape))
            input.Exit();
    }

    public void DrawGUI(SpriteBatch spriteBatch)
    {
        spriteBatch.Begin();
        for (int i = 1; i < backgrounds.Count; i++)
        {
            spriteBatch.Draw(backgrounds[i], positions[i], Color.White);
            spriteBatch.DrawString(fonts[i], text[i], positions[i] + new Vector2(5.0f, 5.0f), Color.White);
        }

        foreach (Button but in buttons)
        {
            but.Draw(spriteBatch);
        }
        drawEntities(spriteBatch);
        spriteBatch.End();
    }

    public void DrawBG(SpriteBatch spriteBatch)
    {
        spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearWrap, null, null, null, camera.getTransformation());
        spriteBatch.Draw(backgrounds[0], positions[0], enviBox, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
        spriteBatch.End();
    }
}
