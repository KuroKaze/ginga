using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Ginga
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static int winWidth = 1280, winHeight = 720, environmentSize = 10000;
        Vector2 playerStart;
        Vector2 computerStart;
        Camera camera;
        Input input;
        Interface inter;
        Collision collision;
        List<Entity> ships;
        Song bgMusic, winMusic, loseMusic;
        Random random;
        int enemyArmySize = 100, allyArmySize = 100;

        Vector2 creditStart = new Vector2(300, winHeight);
        string credit = "Game Design:\n" +
                        "David Chen\n\n" +
                        "Programming:\n" +
                        "David Chen\n\n" +
                        "Art:\n" +
                        "David Chen\n" +
                        "StockImage(Nebula.jpg)\n\n" +
                        "Sound:\n" +
                        "Dvorak 9th Symphony\n" +
                        "Dvorak 8th Symphony\n" +
                        "Beethoven 9th Symphony\n\n" +
                        "Production:\n" +
                        "David Chen\n\n" +
                        "Testers:\n" +
                        "David Chen\n" +
                        "David Wen\n" +
                        "Steven Zeng\n\n" +
                        "Special Thanks:\n" +
                        "David Wen(Mathematical Help)";

        AudioEngine audioEngine;
        WaveBank waveBank;
        SoundBank soundBank;
        Cue cue;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            random = new Random();
            playerStart = new Vector2(random.Next(environmentSize - 500), random.Next(environmentSize - 500));
            computerStart = new Vector2(random.Next(environmentSize - 500), random.Next(environmentSize - 500));
            if (playerStart.X - 250 <= 0 && playerStart.Y - 250 <= 0)
                camera = new Camera(Vector2.Zero);
            else if (playerStart.X - 250 <= 0)
                camera = new Camera(new Vector2(0, playerStart.Y - 250));
            else if (playerStart.Y - 250 <= 0)
                camera = new Camera(new Vector2(playerStart.X - 250, 0));
            else
                camera = new Camera(new Vector2(playerStart.X - 250, playerStart.Y - 250));
            input = new Input(this);
            collision = new GingaCollision(new Rectangle(0, 0, environmentSize, environmentSize),
                                      new Rectangle(2, 2, winWidth - 4, winHeight - 4),
                                      new Rectangle((int)camera.position.X - 25, (int)camera.position.Y - 25, 1180, 620));
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            input.initGraphics(graphics, false, winWidth, winHeight);
            bgMusic = Content.Load<Song>("Music//Dvorak 9");
            winMusic = Content.Load<Song>("Music//Dvorak 8W");
            loseMusic = Content.Load<Song>("Music//Dvorak 8L");
            MediaPlayer.Play(bgMusic);
            MediaPlayer.IsRepeating = true;
            //audioEngine = new AudioEngine("Content//Music//Audio.xgs");
            //waveBank = new WaveBank(audioEngine, "Content//Music//Wave Bank.xwb");
            //soundBank = new SoundBank(audioEngine, "Content//Music//Sound Bank.xsb");
            //cue = soundBank.GetCue("Laser Blasts");
            //cue.Play();

            

            inter = new Environment(Content.Load<Texture2D>("Background//SpaceBG"),
                                    Content.Load<Texture2D>("Units//Select"),
                                    new Rectangle(0, 0, environmentSize, environmentSize), 
                                    input, 
                                    camera, 
                                    collision);
            inter.addComponent(Content.Load<Texture2D>("Background//RightPanel"), 
                               new Vector2(winWidth - 150, 0), 
                               Content.Load<SpriteFont>("Fonts//Arial"));
            inter.addComponent(Content.Load<Texture2D>("Background//BottomPanel"), 
                               new Vector2(0, winHeight - 150), 
                               Content.Load<SpriteFont>("Fonts//Arial"));
            inter.addButton(Content.Load<Texture2D>("Button//Rectangle"), new Vector2(1140, 150));
            ships = new List<Entity>();

            int j = 0, k = 0;

            for (int i = 0; i < allyArmySize; i++)
            {
                ships.Add(new Unit(Content.Load<Texture2D>("Units//Ship"), 2, 2, 0, 1, new Vector2(k * 30.0f + playerStart.X, j * 30.0f + playerStart.Y), false));
                k++;
                if(k > 20)
                {
                    k = 0;
                    j++;
                }
            }
            j = k = 0;

            for (int i = 0; i < enemyArmySize; i++)
            {
                ships.Add(new Unit(Content.Load<Texture2D>("Units//EShip"), 2, 2, 0, 1, new Vector2(k * 30.0f + computerStart.X, j * 30.0f + computerStart.Y), true));
                k++;
                if (k > 20)
                {
                    k = 0;
                    j++;
                }
            }
            collision.add(ships);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (input.keyPressed(Keys.Escape))
                input.Exit();
            
            Vector2 oldMousPos = input.mousePos();
            Rectangle rect = new Rectangle(-1, -1, 0, 0);

            if (allyArmySize != 0 && enemyArmySize != 0)
            {
                for (int i = 0; i < ships.Count; i++)
                {
                    Entity ent = ((GingaCollision)collision).collideClosest(((Unit)ships[i]).range, ((Unit)ships[i]).enemy);
                    if (ent != null && ent.GetType().Equals(ships[i].GetType()))
                    {
                        ((Unit)ships[i]).withinRange = true;
                        ((Unit)ships[i]).turnToEnemy(ent.position);
                        if (!((Unit)ent).withinRange)
                            ((Unit)ent).turnToEnemy(ships[i].position);
                        if (Math.Abs(((Unit)ships[i]).turning - ((Unit)ships[i]).angle) < MathHelper.ToRadians(((Unit)ships[i]).rotateSpeed))
                        {
                            //Console.WriteLine(ships[i].angle.ToString() + ", " + ent.angle.ToString());
                            if (Math.Abs(ships[i].angle - ent.angle) < MathHelper.ToRadians(45) || Math.Abs(ships[i].angle - ent.angle) > MathHelper.ToRadians(315))
                                ((Unit)ent).hp -= 4;
                            else if (Math.Abs(ships[i].angle - ent.angle) < MathHelper.ToRadians(135) || Math.Abs(ships[i].angle - ent.angle) > MathHelper.ToRadians(225))
                                ((Unit)ent).hp -= 2;
                            else
                                ((Unit)ent).hp -= 1;
                        }
                        List<Entity> withinRange = new List<Entity>();
                        if(((Unit)ships[i]).enemy)
                            withinRange = collision.collide(new Circle(ships[i].position, 200f));
                        else
                            withinRange = collision.collide(new Circle(ships[i].position, 30f));
                        withinRange.RemoveAll(
                                delegate(Entity ally)
                                {
                                    return ((Unit)ally).enemy != ((Unit)ships[i]).enemy;
                                });
                        foreach (Entity ally in withinRange)
                        {
                            if (!((Unit)ally).withinRange)
                            {
                                Vector2 direction = new Vector2(((Unit)ships[i]).heading.X - ((Unit)ships[i]).position.X, ((Unit)ships[i]).heading.Y - ((Unit)ships[i]).position.Y);
                                if (((Unit)ships[i]).enemy)
                                    ((Unit)ally).moveTo(ent.position);
                                else if((int)direction.X == 0 && (int)direction.Y == 0)
                                    ((Unit)ally).attack(ent);
                            }
                        }
                        //Console.WriteLine(((Unit)ships[i]).enemyFacing.ToString() + ent.position.ToString());
                    }
                    else
                    {
                        ((Unit)ships[i]).withinRange = false;
                        if (((Unit)ships[i]).enemy)
                        {
                            ((Unit)ships[i]).moveTo(((Unit)ships[i]).facing);
                            Vector2 direction = new Vector2(((Unit)ships[i]).heading.X - ((Unit)ships[i]).position.X, ((Unit)ships[i]).heading.Y - ((Unit)ships[i]).position.Y);
                            if ((int)direction.X == 0 && (int)direction.Y == 0 && random.Next(749) == 0)
                                ((Unit)ships[i]).moveTo(new Vector2(random.Next(environmentSize), random.Next(environmentSize)));
                        }
                        //if (((Unit)ships[i]).enemy)
                        //    Console.WriteLine(((Unit)ships[i]).heading.ToString() + ((Unit)ships[i]).position.ToString());
                        //((Unit)ships[i]).turnTo(((Unit)ships[i]).facing);
                    }
                    ships[i].animate();
                    ships[i].Update(gameTime);



                    if (((Unit)ships[i]).hp <= 0)
                    {
                        if (((Unit)ships[i]).enemy)
                            enemyArmySize--;
                        else
                            allyArmySize--;
                        collision.remove(ships[i]);
                        inter.removeEnity(ships[i]);
                        ships.RemoveAt(i);
                    }
                }
                if (allyArmySize == 0 || enemyArmySize == 0)
                {
                    if (allyArmySize == 0)
                        MediaPlayer.Play(loseMusic);
                    else
                        MediaPlayer.Play(winMusic);
                }
            }
            inter.Update();
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            if (allyArmySize != 0 && enemyArmySize != 0)
            {
                inter.DrawBG(spriteBatch);

                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera.getTransformation());

                foreach (Entity ship in ships)
                {
                    if (!collision.moveOffCamera(ship))
                        ship.Draw(spriteBatch);
                }
                spriteBatch.End();

                inter.DrawGUI(spriteBatch);
                ((Environment)inter).DrawSelectionRect(spriteBatch);
            }
            else if (allyArmySize == 0)
            {
                spriteBatch.Begin();
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts//Arial"), "YOU LOSE", new Vector2(winWidth / 2, 100), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts//Arial"), credit, creditStart, Color.White);
                spriteBatch.End();
                creditStart.Y -= 0.5f;
            }
            else if (enemyArmySize == 0)
            {
                spriteBatch.Begin();
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts//Arial"), "YOU WIN", new Vector2(winWidth / 2, 100), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts//Arial"), credit, creditStart, Color.White);
                spriteBatch.End();
                creditStart.Y -= 0.5f;
            }

            // TODO: Add your drawing code here


            base.Draw(gameTime);
        }
    }
}
