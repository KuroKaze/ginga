﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

class Entity
{
    protected int rows, columns;

    public Texture2D spriteSheet;
    public Color[] textureData;
    public Vector2 position, origin;
    public Tuple<int, int, int> frame;
    public Rectangle srcRect, boundingBox;
    public float angle, scale;
    public Matrix transformM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="texture">Sprite map</param>
    /// <param name="numRows/numColumns">Number of rows and columns of sprites on sprite map (one set of frames for animation per row)</param>
    /// <param name="startFrame/endFrame">Frame no. for starting and ending animation</param>
    /// <param name="loc">Location of object</param>
    /// <param name="ftl">If location is measured from top left corner</param>
    public Entity(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, float scl = 1.0f, float deg = 0.0f)
    {
        spriteSheet = texture;
        rows = numRows;
        columns = numColumns;
        frame = new Tuple<int, int, int>(startFrame, startFrame, endFrame);
        position = new Vector2(loc.X, loc.Y);

        srcRect.Width = spriteSheet.Width / columns;
        srcRect.Height = spriteSheet.Height / rows;
        updateSrcRect();
        angle = MathHelper.ToRadians(deg);
        scale = scl;
        origin = new Vector2(srcRect.Width / 2, srcRect.Height / 2);

        textureData = new Color[srcRect.Width * srcRect.Height];
        spriteSheet.GetData(0, srcRect, textureData, srcRect.X * srcRect.Y, srcRect.Width * srcRect.Height);
        transformM = Matrix.Identity;
        transform(0.0f, 0.0f, 0.0f);
    }

    public Entity(Entity ent)
    {
        spriteSheet = ent.spriteSheet;
        rows = ent.rows;
        columns = ent.columns;
        position = ent.position;
        origin = ent.origin;
        frame = ent.frame;
        textureData = ent.textureData;
        srcRect = ent.srcRect;
        angle = ent.angle;
    }

    public virtual void doWhenCollide()
    {

    }

    public virtual void doWhenClicked()
    {

    }

    public virtual void transform(float x, float y, float degrees, float times = 0.0f)
    {
        angle += MathHelper.ToRadians(degrees);
        if (MathHelper.ToRadians(360) < angle)
            angle -= MathHelper.ToRadians(360);
        else if (0.0f > angle)
            angle += MathHelper.ToRadians(360);
        position.X += x;
        position.Y += y;
        scale += times;

        Matrix shiftToOriginM = Matrix.CreateTranslation(new Vector3(-origin, 0.0f));
        Matrix scaleM = Matrix.CreateScale(scale);
        Matrix rotateM = Matrix.CreateRotationZ(angle);
        Matrix shiftM = Matrix.CreateTranslation(new Vector3(position, 0.0f));
        transformM = shiftToOriginM * scaleM * rotateM * shiftM;

        updateBoundingBox();
    }

    public virtual void transformForward(float forward, float times = 0.0f)
    {
        position.X += (float)Math.Cos((double)angle - MathHelper.ToRadians(90)) * forward;
        position.Y += (float)Math.Sin((double)angle - MathHelper.ToRadians(90)) * forward;
        scale += times;

        Matrix shiftToOriginM = Matrix.CreateTranslation(new Vector3(-origin, 0.0f));
        Matrix scaleM = Matrix.CreateScale(scale);
        Matrix rotateM = Matrix.CreateRotationZ(angle);
        Matrix shiftM = Matrix.CreateTranslation(new Vector3(position, 0.0f));
        transformM = shiftToOriginM * scaleM * rotateM * shiftM;

        updateBoundingBox();
    }

    public virtual void transformToward(float forward, float radians, float times = 0.0f)
    {
        position.X += (float)Math.Cos((double)radians - MathHelper.ToRadians(90)) * forward;
        position.Y += (float)Math.Sin((double)radians - MathHelper.ToRadians(90)) * forward;
        scale += times;

        Matrix shiftToOriginM = Matrix.CreateTranslation(new Vector3(-origin, 0.0f));
        Matrix scaleM = Matrix.CreateScale(scale);
        Matrix rotateM = Matrix.CreateRotationZ(angle);
        Matrix shiftM = Matrix.CreateTranslation(new Vector3(position, 0.0f));
        transformM = shiftToOriginM * scaleM * rotateM * shiftM;

        updateBoundingBox();
    }

    protected void updateBoundingBox()
    {
        Rectangle rectangle = new Rectangle(0, 0, srcRect.Width, srcRect.Height);
        Vector2 leftTop = new Vector2(rectangle.Left, rectangle.Top);
        Vector2 rightTop = new Vector2(rectangle.Right, rectangle.Top);
        Vector2 leftBottom = new Vector2(rectangle.Left, rectangle.Bottom);
        Vector2 rightBottom = new Vector2(rectangle.Right, rectangle.Bottom);

        Vector2.Transform(ref leftTop, ref transformM, out leftTop);
        Vector2.Transform(ref rightTop, ref transformM, out rightTop);
        Vector2.Transform(ref leftBottom, ref transformM, out leftBottom);
        Vector2.Transform(ref rightBottom, ref transformM, out rightBottom);

        Vector2 min = Vector2.Min(Vector2.Min(leftTop, rightTop), Vector2.Min(leftBottom, rightBottom));
        Vector2 max = Vector2.Max(Vector2.Max(leftTop, rightTop), Vector2.Max(leftBottom, rightBottom));

        boundingBox = new Rectangle((int)min.X, (int)min.Y, (int)(max.X - min.X), (int)(max.Y - min.Y));
    }

    public virtual void animate()
    {
        frame = new Tuple<int, int, int>(frame.Item1, frame.Item2 + 1, frame.Item3);
        if (frame.Item2 >= frame.Item3)
            frame = new Tuple<int, int, int>(frame.Item1, frame.Item1, frame.Item3);
        updateSrcRect();
        spriteSheet.GetData(0, srcRect, textureData, srcRect.X * srcRect.Y, srcRect.Width * srcRect.Height);
    }

    public virtual void newAnimation(int startFrame, int endFrame)
    {
        frame = new Tuple<int, int, int>(startFrame, startFrame, endFrame);
    }

    protected void updateSrcRect()
    {
        int row = frame.Item2 / columns;
        int column = frame.Item2 % columns;
        srcRect.X = srcRect.Width * column;
        srcRect.Y = srcRect.Height * row;
    }

    public virtual void Update(GameTime gameTime)
    {

    }

    public virtual void Draw(SpriteBatch spriteBatch)
    {
        spriteBatch.Draw(spriteSheet, position, srcRect, Color.White, angle, origin, scale, SpriteEffects.None, 1);
    }

    public void correctPosition()
    {
        position.X = (int)(position.X);
        position.Y = (int)(position.Y);
    }

    public void debug()
    {
        Console.WriteLine(boundingBox.ToString());
        Console.WriteLine("pos:(" + position.X + ", " + position.Y + ")");
        Console.WriteLine("ori:(" + origin.X + ", " + origin.Y + ")");
        Console.WriteLine("bou:(" + boundingBox.X + ", " + boundingBox.Y + ", " + boundingBox.Width + ", " + boundingBox.Height + ")");
    }
}
