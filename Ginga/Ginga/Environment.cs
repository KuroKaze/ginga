﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Ginga
{
    class Environment : Interface
    {
        Texture2D select;
        List<Entity> selected;
        Rectangle selectRect;
        Vector2 startMousePos, bary = Vector2.Zero;
        //int formation;

        public Environment(Texture2D texture, Texture2D selectTexture, Rectangle environmentBox, Input input, Camera camera, Collision collision)
            : base(texture, environmentBox, input, camera, collision)
        {
            selected = new List<Entity>();
            selectRect = new Rectangle(-1, -1, 0, 0);
            select = selectTexture;
        }

        public void removeEnitity(Entity ent)
        {
            selected.Remove(ent);
        }

        public override void Update()
        {
            base.Update();
            screenScrolling();
            selectingUnits();

            if (input.RMouseButtonClicked() && selected.Count > 0)
            {
                List<Entity> attacked = collision.collide(new Rectangle((int)(input.mousePos().X + camera.position.X), (int)(input.mousePos().Y + camera.position.Y), 1, 1));
                if (attacked.Count == 0)
                {
                    Vector2 vect = (input.mousePos() + camera.position) - bary;
                    foreach (Entity uni in selected)
                        ((Unit)uni).moveTo(vect + uni.position);
                }
                else
                {
                    foreach (Entity uni in selected)
                    {
                        ((Unit)uni).attack(attacked[0]);
                    }
                }
            }


            for(int i = 0; i < buttons.Count; i++)
            {
                if (buttons[i].clicked() && selected.Count > 0)
                {
                    if (i == 0)
                    {
                        squareFormation();
                    }
                }
            }
            
            //Console.WriteLine(selectRect.ToString());
            //Console.WriteLine(mousePos.ToString());
            baryofSelected();
            input.Update();
        }

        public override void drawEntities(SpriteBatch spriteBatch)
        {
            int i = 0, j = 0;
            foreach (Entity ent in entities)
            {
                spriteBatch.Draw(ent.spriteSheet, new Vector2(i * 25.0f + 5.0f, j * 27.0f + 580.0f), new Rectangle(0, 0, ent.srcRect.Width, ent.srcRect.Height), Color.White);
                i++;
                if (i == 45)
                {
                    i = 0;
                    j++;
                }
            }
        }

        protected void baryofSelected()
        {
            bary = Vector2.Zero;
            foreach (Entity uni in selected)
            {
                bary += uni.position;
            }
            bary.X = (int)((bary.X + selected.Count / 2) / selected.Count);
            bary.Y = (int)((bary.Y + selected.Count / 2) / selected.Count);
        }

        protected void screenScrolling()
        {
            Rectangle mousePosition = new Rectangle((int)input.mousePos().X, (int)input.mousePos().Y, 0, 0);
            if (collision.hitLeftScreen(mousePosition) || input.keyHeld(Keys.Left))
                camera.position.X = MathHelper.Clamp(camera.position.X - 10.0f, 0, Ginga.Game1.environmentSize);
            if (collision.hitRightScreen(mousePosition) || input.keyHeld(Keys.Right))
                camera.position.X = MathHelper.Clamp(camera.position.X + 10.0f, 0, Ginga.Game1.environmentSize - 1130);
            if (collision.hitTopScreen(mousePosition) || input.keyHeld(Keys.Up))
                camera.position.Y = MathHelper.Clamp(camera.position.Y - 10.0f, 0, Ginga.Game1.environmentSize);
            if (collision.hitBottomScreen(mousePosition) || input.keyHeld(Keys.Down))
                camera.position.Y = MathHelper.Clamp(camera.position.Y + 10.0f, 0, Ginga.Game1.environmentSize - 570);
            collision.cameraBoundary.X = (int)camera.position.X - 25;
            collision.cameraBoundary.Y = (int)camera.position.Y - 25;
        }

        protected void squareFormation()
        {
            //bool good = true;
            int col = (int)Math.Floor(Math.Sqrt(2.0f * selected.Count));
            int row = (int)(0.5f * col);
            int r = 0, c = 0;
            Vector2 tlc = bary - new Vector2((col / 2) * 30, (row / 2 * 30));
            for (int j = 0; j < row * col; j++)
            {
                Vector2 point = tlc + new Vector2(c * 30, r * 30);
                ((Unit)(selected[j])).moveTo(point);
                c++;
                if (c == col)
                {
                    c = 0;
                    r++;
                }
            }
            if (selected.Count - (row * col) > row)
            {
                for (int j = row * col; j < selected.Count; j++)
                {
                    Vector2 point = tlc + new Vector2(c * 30, r * 30);
                    ((Unit)(selected[j])).moveTo(point);
                    c++;
                    if (c == col)
                    {
                        c = 0;
                        r++;
                    }
                }
            }
            else
            {
                r = 0;
                c = col;
                for (int j = row * col; j < selected.Count; j++)
                {
                    Vector2 point = tlc + new Vector2(c * 30, r * 30);
                    ((Unit)(selected[j])).moveTo(point);
                    r++;
                }
            }
        }

        protected void selectingUnits()
        {
            if (input.lMouseHeld() && input.oldMouseState.LeftButton == ButtonState.Released && (input.mousePos().X < 1130 && input.mousePos().Y < 570) && startMousePos == Vector2.Zero)
                startMousePos = input.mousePos();
            if (input.lMouseHeld() && startMousePos != Vector2.Zero)
            {
                if (input.mousePos().X > 1130 || input.mousePos().Y > 570)
                {
                    if (input.mousePos().X > 1130 && input.mousePos().Y > 570)
                    {
                        selectRect = new Rectangle((int)(startMousePos.X),
                                                (int)(startMousePos.Y),
                                                (int)(1130 - startMousePos.X),
                                                (int)(570 - startMousePos.Y));
                    }
                    else if (input.mousePos().X > 1130)
                    {
                        selectRect = new Rectangle((int)(startMousePos.X),
                                                (int)(startMousePos.Y),
                                                (int)(1130 - startMousePos.X),
                                                (int)(input.mousePos().Y - startMousePos.Y));
                    }
                    else if (input.mousePos().Y > 570)
                    {
                        selectRect = new Rectangle((int)(startMousePos.X),
                                                (int)(startMousePos.Y),
                                                (int)(input.mousePos().X - startMousePos.X),
                                                (int)(570 - startMousePos.Y));
                    }
                }
                else
                {
                    selectRect = new Rectangle((int)(startMousePos.X),
                                            (int)(startMousePos.Y),
                                            (int)(input.mousePos().X - startMousePos.X),
                                            (int)(input.mousePos().Y - startMousePos.Y));
                }
            }
            if (input.lMouseButtonClicked() && (input.mousePos().X < 1130 && input.mousePos().Y < 570))
            {
                Vector2 endMousePos = input.mousePos();
                if (startMousePos.X > endMousePos.X)
                {
                    float temp = startMousePos.X;
                    startMousePos.X = endMousePos.X;
                    endMousePos.X = temp;
                }
                if (startMousePos.Y > endMousePos.Y)
                {
                    float temp = startMousePos.Y;
                    startMousePos.Y = endMousePos.Y;
                    endMousePos.Y = temp;
                }
                selectRect = new Rectangle((int)(startMousePos.X + camera.position.X),
                                        (int)(startMousePos.Y + camera.position.Y),
                                        (int)(endMousePos.X - startMousePos.X),
                                        (int)(endMousePos.Y - startMousePos.Y));
                selected = collision.collide(selectRect);
                selected.RemoveAll(
                    delegate(Entity ent)
                    {
                        return ((Unit)ent).enemy;
                    });
                if (selected.Count > 225)
                    selected.RemoveRange(225, selected.Count - 225);
                removeEntities();
                foreach (Entity ent in selected)
                {
                    addEntityToComponent(ent);
                }
                selectRect = new Rectangle(-1, -1, 0, 0);
                startMousePos = Vector2.Zero;
            }
        }
        public void DrawSelectionRect(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(select, selectRect, Color.White);
            spriteBatch.End();
        }
    }
}
